﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace ICA5
{


    public partial class Form1: Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage (IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture ();

        int tick = 0;
        bool hold = false;
        int[] currentScore;
        int bestScore = 0;
        int gameCount = 3;

        Random rnd = new Random();
        private ImageList diceList = new ImageList();
        
        public Form1 () {
            InitializeComponent();
            diceList.ImageSize = new System.Drawing.Size(45, 45);
            diceList.Images.Add("1", ICA5.Properties.Resources._1);
            diceList.Images.Add("2", ICA5.Properties.Resources._2);
            diceList.Images.Add("3", ICA5.Properties.Resources._3);
            diceList.Images.Add("4", ICA5.Properties.Resources._4);
            diceList.Images.Add("5", ICA5.Properties.Resources._5);
            diceList.Images.Add("6", ICA5.Properties.Resources._6);
        }
        
        private int[] setCurrentQueue(int[] random) {

            int[] currentScore = new int[5];
            for (int i = 0; i < 5; i++) {
                PictureBox tempPictureBox = (PictureBox)this.Controls["pictureBox" + (i + 1).ToString()];
                int tempScore = 0;
                if (tempPictureBox.BackColor == Color.Lime) {
                    tempScore = rnd.Next(6);
                    tempPictureBox.Image = diceList.Images[tempScore];
                } else {
                    tempScore = random[i];
                }
                currentScore[i] = tempScore;
            }

            return currentScore;
        }

        private void setScore (int[] random) {
            score1.Image = diceList.Images[random[0]];
            score2.Image = diceList.Images[random[1]];
            score3.Image = diceList.Images[random[2]];
            score4.Image = diceList.Images[random[3]];
            score5.Image = diceList.Images[random[4]];
        }

        private void setBest (int[] random) {
            best1.Image = diceList.Images[random[0]];
            best2.Image = diceList.Images[random[1]];
            best3.Image = diceList.Images[random[2]];
            best4.Image = diceList.Images[random[3]];
            best5.Image = diceList.Images[random[4]];
        }

        private void timer1_Tick (object sender, EventArgs e) {            
            try {
                currentScore.GetLength(0);
            } catch (Exception) {
                currentScore = new int[5] { this.rnd.Next(6), this.rnd.Next(6), this.rnd.Next(6), this.rnd.Next(6), this.rnd.Next(6) };
            }
            currentScore = setCurrentQueue(currentScore);

            if (tick > 10 && !hold) {
                tick = 0;
                gameCount--;
                timer1.Stop();
                roundButton1.Text = gameCount.ToString();
                if (gameCount == 0)
                    roundButton1.Text = "Play\nAgain";
                System.Diagnostics.Trace.WriteLine(currentScore.Sum() + 5);

                setScore(currentScore);
                int intCurrent = currentScore.Sum() + 5;
                label1.Text = "Score - " + intCurrent;
                if (intCurrent >= bestScore) {
                    bestScore = intCurrent;
                    setBest(currentScore);
                    label2.Text = "Best - " + bestScore;
                }

            } else {
                tick++;
            }
        }

        private void roundButton1_MouseEnter (object sender, EventArgs e) {
            roundButton1.BackColor = Color.FromArgb(255, 255, 222);
            if (gameCount != 0)
                roundButton1.Text = gameCount.ToString();
        }

        private void roundButton1_MouseLeave (object sender, EventArgs e) {
            roundButton1.BackColor = Color.FromArgb(192, 255, 192);
            if (gameCount != 0)
                roundButton1.Text = "Roll";
        }

        private void Form1_MouseDown (object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void roundButton2_Click (object sender, EventArgs e) {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private void roundButton1_MouseDown (object sender, MouseEventArgs e) {            
            if (gameCount == 0) {
                resetProgram();                
            } else {
                timer1.Start();
                tick = 0;
                roundButton1.ResetText();
                hold = true;
            }
        }
        
        private void roundButton1_MouseUp (object sender, MouseEventArgs e) {
            hold = false;
        }

        private void pictureBoxes_Click (object sender, EventArgs e) {
            ((PictureBox)sender).BackColor = ((PictureBox)sender).BackColor == Color.Lime ? Color.Red : Color.Lime;            
        }

        private void resetProgram() {
            for (int i = 0; i < 5; i++) {
                PictureBox thispb = (PictureBox)this.Controls["pictureBox" + (i + 1).ToString()];
                thispb.Image = diceList.Images[0];
                thispb.BackColor = Color.Lime;
            }
            for (int i = 0; i < 5; i++) {
                PictureBox thispb = (PictureBox)this.Controls["score" + (i + 1).ToString()];
                thispb.Image = null;
            }
            for (int i = 0; i < 5; i++) {
                PictureBox thispb = (PictureBox)this.Controls["best" + (i + 1).ToString()];
                thispb.Image = null;
            }
            roundButton1.Text = "Roll";
            label1.Text = "Score";
            label2.Text = "Best";
            gameCount = 3;
            bestScore = 0;
        }
    }

    public class RoundButton: Button
    {
        protected override void OnPaint (System.Windows.Forms.PaintEventArgs e) {
            GraphicsPath grPath = new GraphicsPath();
            grPath.AddEllipse(0, 0, ClientSize.Width, ClientSize.Height);
            this.Region = new System.Drawing.Region(grPath);
            base.OnPaint(e);
        }
    }
}
