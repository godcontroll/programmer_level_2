﻿namespace ICA5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.best5 = new System.Windows.Forms.PictureBox();
            this.best4 = new System.Windows.Forms.PictureBox();
            this.best3 = new System.Windows.Forms.PictureBox();
            this.best2 = new System.Windows.Forms.PictureBox();
            this.best1 = new System.Windows.Forms.PictureBox();
            this.score5 = new System.Windows.Forms.PictureBox();
            this.score4 = new System.Windows.Forms.PictureBox();
            this.score3 = new System.Windows.Forms.PictureBox();
            this.score2 = new System.Windows.Forms.PictureBox();
            this.score1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.roundButton2 = new ICA5.RoundButton();
            this.roundButton1 = new ICA5.RoundButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.best5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.best4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.best3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.best2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.best1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.score5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.score4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.score3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.score2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.score1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(241, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Score";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(241, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Best";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Lime;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Image = global::ICA5.Properties.Resources._1;
            this.pictureBox5.Location = new System.Drawing.Point(162, 53);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(51, 50);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox5.TabIndex = 21;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBoxes_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Lime;
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Image = global::ICA5.Properties.Resources._1;
            this.pictureBox4.Location = new System.Drawing.Point(132, 122);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(51, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 20;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBoxes_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Lime;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = global::ICA5.Properties.Resources._1;
            this.pictureBox3.Location = new System.Drawing.Point(59, 122);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(51, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 19;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBoxes_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Lime;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::ICA5.Properties.Resources._1;
            this.pictureBox2.Location = new System.Drawing.Point(23, 53);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(51, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBoxes_Click);
            // 
            // best5
            // 
            this.best5.Location = new System.Drawing.Point(456, 128);
            this.best5.Name = "best5";
            this.best5.Size = new System.Drawing.Size(47, 47);
            this.best5.TabIndex = 17;
            this.best5.TabStop = false;
            // 
            // best4
            // 
            this.best4.Location = new System.Drawing.Point(403, 128);
            this.best4.Name = "best4";
            this.best4.Size = new System.Drawing.Size(47, 47);
            this.best4.TabIndex = 16;
            this.best4.TabStop = false;
            // 
            // best3
            // 
            this.best3.Location = new System.Drawing.Point(350, 128);
            this.best3.Name = "best3";
            this.best3.Size = new System.Drawing.Size(47, 47);
            this.best3.TabIndex = 15;
            this.best3.TabStop = false;
            // 
            // best2
            // 
            this.best2.Location = new System.Drawing.Point(297, 128);
            this.best2.Name = "best2";
            this.best2.Size = new System.Drawing.Size(47, 47);
            this.best2.TabIndex = 14;
            this.best2.TabStop = false;
            // 
            // best1
            // 
            this.best1.Location = new System.Drawing.Point(244, 128);
            this.best1.Name = "best1";
            this.best1.Size = new System.Drawing.Size(47, 47);
            this.best1.TabIndex = 13;
            this.best1.TabStop = false;
            // 
            // score5
            // 
            this.score5.Location = new System.Drawing.Point(456, 54);
            this.score5.Name = "score5";
            this.score5.Size = new System.Drawing.Size(47, 47);
            this.score5.TabIndex = 12;
            this.score5.TabStop = false;
            // 
            // score4
            // 
            this.score4.Location = new System.Drawing.Point(403, 54);
            this.score4.Name = "score4";
            this.score4.Size = new System.Drawing.Size(47, 47);
            this.score4.TabIndex = 11;
            this.score4.TabStop = false;
            // 
            // score3
            // 
            this.score3.Location = new System.Drawing.Point(350, 54);
            this.score3.Name = "score3";
            this.score3.Size = new System.Drawing.Size(47, 47);
            this.score3.TabIndex = 10;
            this.score3.TabStop = false;
            // 
            // score2
            // 
            this.score2.Location = new System.Drawing.Point(297, 54);
            this.score2.Name = "score2";
            this.score2.Size = new System.Drawing.Size(47, 47);
            this.score2.TabIndex = 9;
            this.score2.TabStop = false;
            // 
            // score1
            // 
            this.score1.Location = new System.Drawing.Point(244, 54);
            this.score1.Name = "score1";
            this.score1.Size = new System.Drawing.Size(47, 47);
            this.score1.TabIndex = 8;
            this.score1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Lime;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::ICA5.Properties.Resources._1;
            this.pictureBox1.Location = new System.Drawing.Point(92, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBoxes_Click);
            // 
            // roundButton2
            // 
            this.roundButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.roundButton2.FlatAppearance.BorderSize = 0;
            this.roundButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundButton2.Image = global::ICA5.Properties.Resources.x;
            this.roundButton2.Location = new System.Drawing.Point(485, 8);
            this.roundButton2.Name = "roundButton2";
            this.roundButton2.Size = new System.Drawing.Size(30, 30);
            this.roundButton2.TabIndex = 23;
            this.roundButton2.UseVisualStyleBackColor = true;
            this.roundButton2.Click += new System.EventHandler(this.roundButton2_Click);
            // 
            // roundButton1
            // 
            this.roundButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.roundButton1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.roundButton1.FlatAppearance.BorderSize = 0;
            this.roundButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundButton1.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roundButton1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.roundButton1.Location = new System.Drawing.Point(92, 64);
            this.roundButton1.Name = "roundButton1";
            this.roundButton1.Size = new System.Drawing.Size(55, 52);
            this.roundButton1.TabIndex = 22;
            this.roundButton1.Text = "Roll";
            this.roundButton1.UseVisualStyleBackColor = false;
            this.roundButton1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.roundButton1_MouseDown);
            this.roundButton1.MouseEnter += new System.EventHandler(this.roundButton1_MouseEnter);
            this.roundButton1.MouseLeave += new System.EventHandler(this.roundButton1_MouseLeave);
            this.roundButton1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.roundButton1_MouseUp);
            // 
            // Form1
            // 
            this.AcceptButton = this.roundButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.CancelButton = this.roundButton2;
            this.ClientSize = new System.Drawing.Size(530, 188);
            this.Controls.Add(this.roundButton2);
            this.Controls.Add(this.roundButton1);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.best5);
            this.Controls.Add(this.best4);
            this.Controls.Add(this.best3);
            this.Controls.Add(this.best2);
            this.Controls.Add(this.best1);
            this.Controls.Add(this.score5);
            this.Controls.Add(this.score4);
            this.Controls.Add(this.score3);
            this.Controls.Add(this.score2);
            this.Controls.Add(this.score1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Opacity = 0.9D;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Form1";
            this.TopMost = true;
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.best5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.best4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.best3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.best2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.best1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.score5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.score4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.score3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.score2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.score1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox score1;
        private System.Windows.Forms.PictureBox score2;
        private System.Windows.Forms.PictureBox score3;
        private System.Windows.Forms.PictureBox score4;
        private System.Windows.Forms.PictureBox score5;
        private System.Windows.Forms.PictureBox best5;
        private System.Windows.Forms.PictureBox best4;
        private System.Windows.Forms.PictureBox best3;
        private System.Windows.Forms.PictureBox best2;
        private System.Windows.Forms.PictureBox best1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private RoundButton roundButton1;
        private RoundButton roundButton2;
    }
}

