﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GDIDrawer;

namespace ICA6
{
    public partial class Form1: Form
    {
        CDrawer gd = new CDrawer(600, 600);
        int currentSpeed = 20;
        int currentX = 300;
        int currentY = 300;
        readonly int r = 10;
        Color currentColor = Color.Red;
        bool drawBorder = false;

        public Form1 () {
            InitializeComponent();
            gd.ContinuousUpdate = false;
            Rend();
        }

        private void Rend() {
            // Draw
            if (drawBorder) {
                gd.AddEllipse(currentX, currentY, 2 * r, 2 * r, currentColor, 1, Color.White);
            } else {
                gd.AddEllipse(currentX, currentY, 2 * r, 2 * r, currentColor);
            }
            gd.Render();
        }

        private void Form1_KeyDown (object sender, KeyEventArgs e) {
            bool doRend = false;
            // Speed

            if (e.KeyCode.Equals(Keys.ShiftKey)) {
                currentSpeed = 40;
            }
            if (e.KeyCode.Equals(Keys.ControlKey)) {
                currentSpeed = 60;
            }
            if (e.Alt) {
                currentSpeed = 80;
            }

            // Movement
            if (e.KeyCode.Equals(Keys.Up)) {
                currentY -= currentSpeed;
                doRend = true;
            } else if (e.KeyCode.Equals(Keys.Down)) {
                currentY += currentSpeed;
                doRend = true;
            } else if (e.KeyCode.Equals(Keys.Left)) {
                currentX -= currentSpeed;
                doRend = true;
            } else if (e.KeyCode.Equals(Keys.Right)) {
                currentX += currentSpeed;
                doRend = true;
            }

            // Color
            if (e.KeyCode.Equals(Keys.B)) {
                currentColor = Color.Blue;
            }
            if (e.KeyCode.Equals(Keys.R)) {
                currentColor = Color.Red;
            }
            if (e.KeyCode.Equals(Keys.G)) {
                currentColor = Color.Green;
            }

            // Etc
            if (e.KeyCode.Equals(Keys.X) || e.KeyCode.Equals(Keys.Escape)) {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            if (e.KeyCode.Equals(Keys.F1)) {
                drawBorder = true;
            }

            // Draw
            if (doRend) {
                Rend();
                System.Diagnostics.Trace.WriteLine("Hi");
            }
        }

        private void Form1_KeyUp (object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.F1)) {
                drawBorder = false;
            }
        }
    }
}
