﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ICA4
{
    public partial class Form1: Form
    {
        List<System.Uri> urlHistory = new List<System.Uri>();
        bool isBacked = false;
        int currentIndex = 0;

        public Form1 () {
            InitializeComponent();
            urlHistory.Add(new System.Uri("https://www.google.com/?gws_rd=ssl"));
        }

        private void textBox1_TextChanged (object sender, EventArgs e) {            
            button2.Enabled = (textBox1.TextLength > 0) ? true : false;
            
        }

        private void webBrowser1_Navigated (object sender, WebBrowserNavigatedEventArgs e) {
            urlHistory.Add(webBrowser1.Url);
            if (!isBacked) {
                currentIndex = urlHistory.Count - 1;
                System.Diagnostics.Trace.Write("Hi");
            }
            textBox1.Text = webBrowser1.Url.ToString();
            if (!textBox1.Text.Equals(listBox1.Items[0])) {
                listBox1.Items.Insert(0, textBox1.Text);
                button1.Enabled = true;
            }
            isBacked = false;
        }

        private void button2_Click (object sender, EventArgs e) {
            webBrowser1.Url = new System.Uri(textBox1.Text.Contains("http") ? textBox1.Text : "https://www.google.com/search?q=" + textBox1.Text);            
        }

        private void textBox1_KeyDown (object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) button2.PerformClick();
        }

        private void button1_Click (object sender, EventArgs e) {
            if (currentIndex > 0)
                currentIndex--;
            webBrowser1.Url = urlHistory[currentIndex];
            isBacked = true;
        }

        private void listBox1_SelectedIndexChanged (object sender, EventArgs e) {
            if (listBox1.SelectedIndex != 0) {                
                string temp = listBox1.Text;
                listBox1.Items.Remove(listBox1.SelectedItem);
                listBox1.SetSelected(0, true);
                textBox1.Text = temp;
                button2.PerformClick();
            }
        }
    }
}
